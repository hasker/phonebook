#
# DockerFile for exercise 7: cucumber with Java 8
#   Use: docker build ./
#

# docker build --progress=plain
FROM ubuntu:18.04
# install cucumber, junit4
# wget
RUN apt-get update; apt-get install -y openjdk-11-jdk
RUN apt-get install -y junit4 git maven
RUN apt-get install -y cucumber
# RUN bash /do-install
# switch to java 8
# RUN update-alternatives --set java /usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java
COPY . /container
# run tests
RUN cd /container; bash ./run-cucumber
