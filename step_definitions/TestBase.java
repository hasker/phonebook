// TestBase: base class for testing
// RWHasker, 2018

package step_definitions;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import cucumber.api.java8.En;

import pbook.*;

public class TestBase implements En {

  // return output from main given this call
  // commandArg is optional; if null, no argument is specified
  protected String[] outputLinesFromMainFor(AbstractMain mainObject,
                                            String commandArg,
                                            String inputText) {
    ByteArrayOutputStream capture = new ByteArrayOutputStream();
    PrintStream ps = new PrintStream(capture);
    PrintStream savedSystemOutput = System.out;
    System.setOut(ps);
    
    InputStream is = null;
    try {
      is  = new ByteArrayInputStream(inputText.getBytes("UTF-8"));
    } catch (UnsupportedEncodingException e) {
      System.err.print("Fatal error: " + e);
      System.exit(1);
    }
    InputStream savedSystemInput = System.in;
    System.setIn(is);
    
    if ( commandArg == null )
      mainObject.abstractMain();
    else {
      String[] args = new String[1];
      args[0] = commandArg;
      mainObject.abstractMain(args);
    }

    // Put things back
    System.out.flush();
    System.setOut(savedSystemOutput);
    System.setIn(savedSystemInput);

    String lines[] = capture.toString().split("[\r\n]+");

    return lines;
  }

  public static void reportOutput(String[] lines) {
    System.out.println("Results:");
    for(String aLine : lines) {
      System.out.println("'" + aLine + "'");
    }
  }
}
