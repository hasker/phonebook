// BasePhones: step definitions for baseline phones testing (no undo/redo)
// RWHasker, Oct. 2018

package step_definitions;

import cucumber.api.java8.En;
import cucumber.runtime.java8.LambdaGlueBase;
import cucumber.api.PendingException;
import static org.junit.Assert.*;

import pbook.*;

public class BasePhones extends TestBase {

  private String inputHeight = null;
  private String[] resultLines = null;

  public BasePhones() {
    When("^I reset the phonebook$", () -> {
        String[] ignored = outputLinesFromMainFor(new Phones(), "reset", "");
      });
    
    When("^I add ([a-zA-Z]+) with number (\\d+)$", (String name, Integer number) -> {
        String input = name + "\n" + number.toString();
        String[] result = outputLinesFromMainFor(new Phones(), "add", input);
      });
    
    When("^I undo$", () -> {
        String[] ignored = outputLinesFromMainFor(new Phones(), "undo", "");
      });

    When("^I redo$", () -> {
        String[] ignored = outputLinesFromMainFor(new Phones(), "redo", "");
      });

    Then("^([a-zA-Z]+)'s number is (\\d+)$", (String name, Integer number) -> {

        String input = name;
        String[] results = outputLinesFromMainFor(new Phones(), "get", input);
        //reportOutput(results);
        assertTrue(results.length > 1);
        assertEquals("Phone # for " + name + ": " + number, results[1]);
      });

    Then("^the number of entries is (\\d+)$", (Integer count) -> {
        String[] results = outputLinesFromMainFor(new Phones(), "list", "");
        //System.out.println("Expecting " + count + " results:");
        //reportOutput(results);
        if ( count.intValue() == 0 ) {
          assertEquals(1, results.length);
          assertEquals("", results[0]);
        } else {
          assertEquals(results.length, count.intValue());
        }
      });
  }
}
