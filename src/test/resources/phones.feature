Feature: core phones feature file

Scenario: add a person and list
  When I reset the phonebook
  And I add Xander with number 123
  Then Xander's number is 123
  And the number of entries is 1

Scenario: add a person and reset
  When I reset the phonebook
  And I add Xander with number 123
  And I reset the phonebook
  Then the number of entries is 0

Scenario: add a person and undo
  When I reset the phonebook
  And I add Xander with number 123
  And I undo
  Then the number of entries is 0

Scenario: add a person and undo then redo
  When I reset the phonebook
  And I add Xander with number 123
  And I undo
  And I redo
  Then the number of entries is 1
